<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$conf = Mage::getConfig();

/**
 * Crons
 */
$conf->saveConfig('hickeys_epos/cron_schedule/hickeys_export_click_and_collect_payments', '*/2 * * * *', 'default', 0);
$conf->saveConfig('hickeys_epos/cron_schedule/hickeys_import_click_and_collect_arrivals', '*/3 * * * *', 'default', 0);

/**
 * Paths
 */
$conf->saveConfig('hickeys_epos/ftp/payments_path', '/payments/', 'default', 0);
$conf->saveConfig('hickeys_epos/ftp/arrivals_path', '/arrivals/', 'default', 0);

/**
 * File extensions
 */
$conf->saveConfig('hickeys_epos/file_extensions/payments', 'P{branch-number}', 'default', 0);
$conf->saveConfig('hickeys_epos/file_extensions/branches', 'B{branch-number}', 'default', 0);

/**
 * Click and collect message text
 */
$conf->saveConfig('hickeys_clickandcollect/message/from', "+35316458200", 'default', 0);
$conf->saveConfig('hickeys_clickandcollect/message/arrived', "Hello {customer-name},
your reservation {order-number} is ready to be collected.

A proof of ID may be required.
", 'default', 0);


$installer->endSetup();