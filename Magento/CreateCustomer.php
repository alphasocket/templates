<?php
require_once 'Mage.php';
/*
 * Create a Test user for testing magento checkout
 *
 * http://inchoo.net/magento/programming-magento/programmaticaly-adding-new-customers-to-the-magento-store/
 */
try{
	$websiteId = Mage::app()->getWebsite()->getId();
	$store = Mage::app()->getStore();

	$customer = Mage::getModel("customer/customer");
	$customer
		->setWebsiteId($websiteId)
		->setStore($store)
		->setFirstname('Customer')
		->setMiddleName('Checkout')
		->setLastname('Test')
		->setEmail('samuel@studioforty9.com')
		->setPassword('checkouttesting')
	;

	$address = Mage::getModel("customer/address");
	$address
		->setCustomerId($customer->getId())
		->setFirstname($customer->getFirstname())
		->setMiddleName($customer->getMiddlename())
		->setLastname($customer->getLastname())
		->setCountryId('IE')
		//->setRegionId('1') //state/province, only needed if the country is USA
		->setPostcode('T12 ND73')
		->setCity('Cork')
		->setTelephone('0212392349')
		->setCompany('Studioforty9')
		->setStreet('South mall')
		->setIsDefaultBilling('1')
		->setIsDefaultShipping('1')
		->setSaveInAddressBook('1')
	;

	$customer->save();
    $address->save();
}
catch (Exception $e) {
    Zend_Debug::dump($e->getMessage());
}

?>