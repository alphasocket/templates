<?php
$installer = $this;

$installer->startSetup();

$statusTable = $installer->getTable('sales/order_status');
$statusStateTable = $installer->getTable('sales/order_status_state');
$helper = Mage::helper('hickeys_clickandcollect');

/** Insert statuses */
$installer->getConnection()->insertArray(
    $statusTable,
    array(
        'status',
        'label'
    ),
    array(
        array('status' => $helper::ORDER_STATUS_IN_TRANSIT, 'label' => 'In transit from warehouse to branch'),
        array('status' => $helper::ORDER_STATUS_AVAILABLE_IN_STORE, 'label' => 'Order available in store'),
        array('status' => $helper::ORDER_STATUS_WAITING_FOR_COLLECTION, 'label' => 'Waiting the customer for collection'),
    )
);

/** Insert states */
$installer->getConnection()->insertArray(
    $statusStateTable,
    array(
        'status',
        'state',
        'is_default'
    ),
    array(
        array(
            'status' => $helper::ORDER_STATUS_IN_TRANSIT,
            'state' => $helper::ORDER_STATE_IN_TRANSIT,
            'is_default' => 0
        ),
        array(
            'status' => $helper::ORDER_STATUS_AVAILABLE_IN_STORE,
            'state' => $helper::ORDER_STATE_AVAILABLE_IN_STORE,
            'is_default' => 0
        ),
        array(
            'status' => $helper::ORDER_STATUS_WAITING_FOR_COLLECTION,
            'state' => $helper::ORDER_STATE_WAITING_FOR_COLLECTION,
            'is_default' => 0
        ),
    )
);

$installer->endSetup();