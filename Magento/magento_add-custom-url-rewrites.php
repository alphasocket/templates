<?php

/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();

$storeCode2storeId = array();

$storeIdCollection = Mage::getModel('core/store')->getCollection()
    ->addFieldToSelect('*');

foreach ($storeIdCollection as $storeId) {
    $storeCode2storeId[$storeId['code']] = $storeId['group_id'];
}

$storeCode2redirect = array(
    /**
     * Australia
     * New Zealand
     */
    'au' => array(
        'from' => "contacts",
        'to' => "customer-service/contact-us"
    ),

    /**
     * Belgium
     * Denmark
     *
     */
    'nl' => array(
        'from' => "contacts",
        'to' => "hulp/contact"
    ),

    /**
     * Canada
     */
    'ca' => array(
        'from' => "contacts",
        'to' => "customer-service/contact-us"
    ),

    /**
     * Finland
     * Sweden
     * Norway
     */
    'se' => array(
        'from' => "contacts",
        'to' => "customer-service/contact-us"
    ),

    /**
     * France
     */
    'fr' => array(
        'from' => "contacts",
        'to' => "aide/nous-contacter"
    ),

    /**
     * Germany
     * Switzerland
     */
    'de' => array(
        'from' => "contacts",
        'to' => "kundenservice/allgemeine-geschaeftsbedingungen"
    ),

    /**
     * Ireland
     */
    'ie' => array(
        'from' => "contacts",
        'to' => "customer-service/contact-us"
    ),

    /**
     * USA
     * South america
     */
    'us' => array(
        'from' => "contacts",
        'to' => "customer-service/contact-us"
    ),

    /**
     * UK
     */
    'uk' => array(
        'from' => "contacts",
        'to' => "customer-service/contact-dubarry"
    ),

    /**
     * Rest of world
     */
    'eu' => array(
        'from' => "contacts",
        'to' => "customer-service/contact-us/"
    ),

);


try {
    foreach ($storeCode2redirect as $storeCode => $customRedirect) {
        // Unique ID
        $idPath = strtoupper($storeCode)."_contacts_to_contacts_cms_page";

        // Permanent redirect
        $redirectionType = 'RP';

        // Save redirect
        Mage::getModel('core/url_rewrite')
            ->setIdPath($idPath)
            ->setOptions($redirectionType)
            ->setStoreId($storeCode2storeId[$storeCode])
            ->setIsSystem(0)
            ->setRequestPath($customRedirect['from'])
            ->setTargetPath($customRedirect['to'])
            ->save();
    }
} catch (Exception $e) {
    Mage::logException($e);
}

$this->endSetup();